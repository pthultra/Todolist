from datetime import datetime
from idlelib.configdialog import is_int

from django.shortcuts import render, redirect
from .models import Todo
from .forms import NewTodoForm
from django.contrib.auth.decorators import permission_required, login_required


# Create your views here.

@login_required
def home(request):
    if request.method == 'POST':
        for i in request.POST:
            if is_int(i):
                if len(request.POST[i]) == 2 or len(request.POST[i]) == 4:
                    t = Todo.objects.get(id=int(i))
                    t.status = not t.status
                    t.save()
                    return redirect(home)
    high = Todo.objects.filter(user=request.user, priority='2', status=0)
    medium = Todo.objects.filter(user=request.user, priority='1', status=0)
    low = Todo.objects.filter(user=request.user, priority='0', status=0)
    completed = Todo.objects.filter(user=request.user, status=1)
    context = {'high': high, 'medium': medium, 'low': low, 'completed': completed}
    return render(request, 'todolist/home.html', context)


@login_required
def add_todo(request):
    if request.method == 'POST':
        form = NewTodoForm(request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.user = request.user
            item.creation_date = datetime.now()
            item.save()
            return redirect('home')
    else:
        form = NewTodoForm()
    return render(request, 'todolist/add.html', {'form': form})


def change_status(id, status, post_info):
    t = Todo.objects.get(id=id)
    if len(post_info) == 2 or len(post_info) == 4:
        t.status = not t.status
        t.save()
