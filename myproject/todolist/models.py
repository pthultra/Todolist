from django import forms
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
# Create your models here.

AUTH_USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth_User')

class Todo(models.Model):
    STATUS_CHOICES = [(False, 'new'), (True, 'done')]
    PRIORITY_CHOICES = [('0', 'low'), ('1', 'medium'), ('2', 'high')]
    status = models.BooleanField(choices=STATUS_CHOICES, default=False)
    priority = models.CharField(max_length=10, choices=PRIORITY_CHOICES, default='low')
    creation_date = models.DateTimeField
    title = models.CharField(max_length=30)
    user = models.ForeignKey(User, default=0, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
